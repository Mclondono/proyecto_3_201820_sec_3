package test.model.data_structures;
//TODO completar tests
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import src.model.data_structures.RedBlackBST;

public class RedBlackBSTTest {

	private RedBlackBST<String, String> traductor;
	@Test
	public void testConstructor()
	{
		traductor = new RedBlackBST<>();
		assertNotNull(traductor);
	}
	@Test
	public void testPut()
	{
		traductor = new RedBlackBST<>();
		traductor.put("pez", "fish");
		assertTrue(traductor.contains("pez"));
		traductor.put("perro", "dog");
		assertTrue(traductor.contains("perro"));
		traductor.put("gato", "cat");
		assertTrue(traductor.contains("gato"));
	}
	@Test
	public void testGet()
	{
		traductor = new RedBlackBST<>();
		traductor.put("pez", "fish");
		traductor.put("perro", "dog");
		traductor.put("gato", "cat");
		
		String valor = traductor.get("pez");
		assertEquals(valor, "fish");
		valor = traductor.get("gato");
		assertEquals(valor, "cat");
		valor = traductor.get("perro");
		assertEquals(valor , "dog");
	}	

	@Test
	public void testDelete()
	{
		traductor = new RedBlackBST<>();
		traductor.put("pez", "fish");
		traductor.put("perro", "dog");
		traductor.put("gato", "cat");
		
		traductor.delete("gato");
		assertFalse(traductor.contains("gato"));
		traductor.delete("perro");
		assertFalse(traductor.contains("perro"));
		traductor.delete("pez");
		assertFalse(traductor.contains("pez"));
		
	}
}
