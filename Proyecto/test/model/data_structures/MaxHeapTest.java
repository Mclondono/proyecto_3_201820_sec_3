package test.model.data_structures;

import junit.framework.TestCase;
import src.model.data_structures.MaxHeap;



public class MaxHeapTest extends TestCase
{

    private MaxHeap<Integer> heap;

    
    private int size;

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    
    /**
     * Construye una lista  vacia
     * 
     */
    private void setupEscenario1( )
    {
        heap = new MaxHeap<Integer>();
        size = 0;

    }

    /**
     * Construye una nueva lista con 10 elementos
     * 
     */
    private void setupEscenario2( )
    {
    	heap = new MaxHeap<Integer>();
        size = 10;
        for( int cont = 0; cont < size; cont++ )
        {
            heap.insert( cont);
        }
        heap.insert(17);
        heap.insert(16);
        heap.insert(34);
        heap.insert(11);
        size=14;
    }

    /**
     * Construye una nueva lista con 10 elementos
     * 
     */
    private void setupEscenario3( )
    {
    	heap = new MaxHeap<Integer>();
        size = 10;
        for( int cont = size-1; cont >= 0; cont-- )
        {
            heap.insert( cont);
        }
        heap.insert(17);
        heap.insert(9);
        heap.insert(34);
        heap.insert(12);
        heap.insert(28);
        heap.insert(0);

        size=16;
    }

    /**
     * Prueba estructura
     * 
     **/
    public void testEstructuraMaxHeap( )
    {
        setupEscenario3( );
        
        // Verifica que la informaci�n sea correcta
        
        Integer padre;
        Integer hijo1;
        Integer hijo2;
        
        for(int i=1; i<size;i++){
        	padre=heap.get(i);
        	hijo1=heap.get(i*2);
        	hijo2=heap.get((i*2)+1);
        	if(hijo1!=null&&hijo2!=null){
        		int comparadorHijo1= padre.compareTo(hijo1);
        		int comparadorHijo2= padre.compareTo(hijo2);
        		assertTrue( "La estructura no cumple las condiciones del Heap binario", comparadorHijo1>=0&&comparadorHijo2>=0 );
        	}
        }
       
        // Verifica que el tamaño
        assertEquals( "El tamaño del Heap Binario no es el correcto", size, heap.size( ) );
    
    }
    /**
     * Prueba add
     * 
     **/
    public void testinsert( )
    {
        setupEscenario3( );
        
        // Verifica que la informaci�n sea correcta
        assertEquals( "La adici�n no se realiz� correctamente", heap.get(1) , (Integer) 34 );
        
        assertEquals( "La adici�n no se realiz� correctamente", heap.get(heap.size()) , (Integer) 0 );
        
       
        // Verifica que el tamaño
        assertEquals( "El tamaño del Heap Binario no es el correcto", size, heap.size( ));
    
    }


    /**
     * Prueba delete Max
     * */
    public void testdeleteMax( )
    {
    	setupEscenario2( );
    	int t=0;
    	assertEquals(  "No se obtuvo el elemento indicado", heap.get(1), (Integer) 34);
    	
    	heap.delMax();
           // Verifica que la informaci�n sea correcta
            assertTrue( "No se borro correctamente", heap.get(1)!=34);
            
         t=heap.get(1);
         heap.delMax();
            assertTrue( "No se borro correctamente", heap.get(1)!=t);
            
          t=heap.get(1);
          heap.delMax();
          assertTrue( "No se borro correctamente", heap.get(1)!=t);
              
          

    }

    /**
     * Prueba get
     * 
    */
    public void testGetPorPosicion( )
    {
    	setupEscenario2( );
        

        
            // Verifica que la informaci�n sea correcta
        	assertEquals(  "No se obtuvo el elemento indicado", heap.get(1), (Integer) 34);
        	assertTrue(  "No se obtuvo el elemento indicado", heap.get(size).getClass().equals(Integer.class));
            
            
            // Verifica que el tamaño
            assertEquals( "El tamaño de la lista no es el correcto", size, heap.size( ) );
         

   
    }
    
    /**
     * Prueba get Max
     * 
    */
    public void testGetPorElemento( )
    {
    	setupEscenario3( );
        
    	// Verifica que la informaci�n sea correcta
            assertTrue( "No se obtuvo el elemento indicado", heap.get(1)!=null);
            
           
            
            // Verifica que el tamaño
            assertEquals( "El tamaño de la lista no es el correcto", size, heap.size() );
         

   
    }
    /**
     * Prueba size
     * 
     * */
    public void testSize( )
    {
        setupEscenario2( );
        
        
     // Verifica que el tamaño
        assertEquals( "El tamaño de la lista no es el correcto", size, heap.size( ) );
     
        heap.delMax();
        
     // Verifica que el tamaño
        assertEquals( "El tamaño de la lista no es el correcto", size-1, heap.size( ) );
      
    }

    
}