package test.model.data_structures;
import org.junit.Test;

import src.model.data_structures.ListQueue;

import static org.junit.Assert.*;
import org.junit.Before;


public class QueueTest {

	private ListQueue<Integer> list;
	@Before
	public void setUp1()
	{
		list = new ListQueue<>();
	}
	
	@Test
	public void testIsEmpty() {
		assertTrue(list.isEmpty());
	}
	@Test
	public void testsize() {
		assertEquals(0, list.size());
	}
	@Test
	public void testEnqueue() {
		list.enqueue(1);
		assertEquals(1, list.size());
	}
	@Test
	public void testDequeue() throws Exception {
		list.enqueue(1);
		list.dequeue();
		assertTrue(list.isEmpty());
	}
}
