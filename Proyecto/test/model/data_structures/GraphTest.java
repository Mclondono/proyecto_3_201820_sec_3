package test.model.data_structures;
import org.junit.Test;

import src.model.data_structures.Graph;

import static org.junit.Assert.*;
import org.junit.Before;

public class GraphTest 
{

	private Graph<Integer, String, String> grafo;

	@Before
	public void setUp1()
	{
		grafo = new Graph<>();
	}

	/**
	 * Tests an empty graph
	 */

	@Before
	public void setUp2()
	{
		grafo = new Graph<>();
		for (int i = 0; i < 50; i++) 
		{
			grafo.addVertex(i, "Hola Mundo " + i);
		}
	}

	@Before
	public void setUp3()
	{
		grafo = new Graph<>();
		for (int i = 0; i < 50; i++) 
		{
			grafo.addVertex(i, "Hola Mundo " + i);
		}
		for (int i = 0; i < 49; i++) 
		{
			grafo.addEdge(i, i+1, i + "/" + (i+1));
		}
	}

	@Test
	public void testGraph()
	{
		setUp1();
		assertEquals(0, grafo.V());
		assertEquals(0, grafo.E());
	}



	/**
	 * Con este test se prueban los métodos de añadir vertices y contar su tamaño.
	 */
	@Test
	public void testAddVertex()
	{
		setUp2();
		assertEquals(50, grafo.V());
	}

	@Test
	public void testAddEdge()
	{
		setUp3();
		assertEquals(50, grafo.V());
	}

	@Test
	public void testGetInfoVertex()
	{
		setUp2();
		assertTrue(grafo.getInfoVertex(30).equals("Hola Mundo 30"));
	}

	@Test
	public void testSetInfoVertex()
	{
		setUp2();
		grafo.setInfoVertex(28, "Tau is better than Pi");
		assertTrue(grafo.getInfoVertex(28).equals("Tau is better than Pi"));
	}

	@Test
	public void testGetInfoEdge()
	{
		setUp3();
		String ans = grafo.getInfoArc(28, 29);
		assertTrue(ans.equals(28 + "/" + 29));
	}

	@Test
	public void testSetInfoArc()
	{
		setUp3();
		grafo.setInfoArc(28, 29, "Hello World!");
		assertTrue(grafo.getInfoArc(28, 29).equals("Hello World!"));
	}

}
