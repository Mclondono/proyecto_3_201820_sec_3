package test.model.data_structures;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import src.model.data_structures.LinearProbing;

public class LinearProbingTest {
	
	private LinearProbing <String, String> traductor;
	@Test
	public void testConstructor()
	{
		traductor = new LinearProbing<>(7);
		assertNotNull(traductor);
	}
	@Test
	public void testPut()
	{
		traductor = new LinearProbing<>(7);
		traductor.put("pez", "fish");
		assertTrue(traductor.contains("pez"));
		traductor.put("perro", "dog");
		assertTrue(traductor.contains("perro"));
		traductor.put("gato", "cat");
		assertTrue(traductor.contains("gato"));
	}
	@Test
	public void testGet()
	{
		traductor = new LinearProbing<>(7);
		traductor.put("pez", "fish");
		traductor.put("perro", "dog");
		traductor.put("gato", "cat");
		
		String valor = traductor.get("pez");
		assertEquals(valor, "fish");
		valor = traductor.get("gato");
		assertEquals(valor, "cat");
		valor = traductor.get("perro");
		assertEquals(valor , "dog");
	}	

	@Test
	public void testDelete()
	{
		traductor = new LinearProbing<>(7);
		traductor.put("pez", "fish");
		traductor.put("perro", "dog");
		traductor.put("gato", "cat");
		
		traductor.delete("gato");
		assertFalse(traductor.contains("gato"));
		traductor.delete("perro");
		assertFalse(traductor.contains("perro"));
		traductor.delete("pez");
		assertFalse(traductor.contains("pez"));
		
	}
	

}
