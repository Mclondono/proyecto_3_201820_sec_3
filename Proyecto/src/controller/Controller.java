package src.controller;

import src.api.IDivvyTripsManager;
import src.model.vo.VOStrongConnectedComponent;
import src.model.vo.VOConnectedComponent;
import src.model.vo.VOPath;
import src.model.vo.VOStation;
import src.model.data_structures.Digraph;
import src.model.data_structures.DoublyLinkedList;
import src.model.data_structures.IDoublyLinkedList;
import src.model.data_structures.IGraph;
import src.model.data_structures.IList;
import src.model.logic.DivvyTripsManager;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static IDivvyTripsManager manager =new DivvyTripsManager();

	//Carga El sistema
	public static void cargarSistema(String rt, String rs)
	{
		 manager.cargarSistema(rt,rs);
	}
	
	//A1
	public static VOPath A1_menorDistancia(double latInicial, double lonInicial, double latFinal, double lonFinal){
		return manager.A1_menorDistancia(latInicial,lonInicial,latFinal,lonFinal);
	}
	
	//A2
	public static VOPath A2_menorNumVertices(double latInicial, double lonInicial, double latFinal, double lonFinal){
		return manager.A2_menorNumVertices(latInicial,lonInicial,latFinal,lonFinal);
	}
	
	//B1
	public static DoublyLinkedList<VOStation> B1_estacionesCongestionadas(int n){
		return manager.B1_estacionesCongestionadas(n);
	}

	//B2
	public static IDoublyLinkedList<VOPath> B2_rutasMinimas(DoublyLinkedList<VOStation> estaciones){
		return manager.B2_rutasMinimas(estaciones);
	}
	
	//C1
	public static Digraph C1_grafoEstaciones(){
		Digraph grafoEstaciones = manager.C1_grafoEstaciones();
		return grafoEstaciones;
	}
	
	//C2
	public static DoublyLinkedList<VOConnectedComponent> C2_componentesFuertementeConectados(Digraph grafo1c){
		return manager.C2_componentesFuertementeConectados(grafo1c);
	}
	
	//C3
	public static void C3_pintarGrafoEstaciones(Digraph grafoEstaciones){
		manager.C3_pintarGrafoEstaciones(grafoEstaciones);
	}
}
