package src.model.logic;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import src.model.data_structures.ArcoDirigido;


import src.api.IDivvyTripsManager;
//import controller.Controller.ResultadoCampanna;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import src.model.vo.VOBike;
import src.model.vo.VOStrongConnectedComponent;
import src.model.vo.VOConnectedComponent;
import src.model.vo.VOEdge;
import src.model.vo.VOPath;
import src.model.vo.VOPoint;
import src.model.vo.VOStation;
import src.model.vo.VOTrip;
import src.model.vo.VOTripsEntreEstaciones;
import src.model.data_structures.IDoublyLinkedList;
import src.model.data_structures.IGraph;
import src.model.data_structures.IList;
import src.model.data_structures.Iter;
import src.model.data_structures.LinearProbing;
import src.model.data_structures.ListQueue;
import src.model.data_structures.MaxHeap;
import src.model.data_structures.Node;
import src.model.data_structures.PrimMST;
import src.model.data_structures.RedBlackBST;
import src.model.data_structures.WeightedDigraph;
import src.model.data_structures.WeightedGraph;
import src.model.data_structures.Arco;
import src.model.data_structures.BFS;
import src.model.data_structures.Bag;
import src.model.data_structures.Dijkstra;
import src.model.data_structures.DoublyLinkedList;
import src.model.data_structures.Graph;
import org.json.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import src.model.data_structures.Kosaraju;
import src.model.data_structures.Digraph;


public class DivvyTripsManager implements IDivvyTripsManager {


	//Ruta del archivo de datos 2017-Q1
	public static final String TRIPS_Q1 = "./data/Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	public static final String TRIPS_Q2 = "./data/Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	public static final String TRIPS_Q3 = "./data/Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4	
	public static final String TRIPS_Q4 = "./data/Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de stations 2017-Q1-Q2	
	public static final String STATIONS_Q1_Q2 = "./data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	public static final String STATIONS_Q3_Q4 = "./data/Divvy_Stations_2017_Q3Q4.csv";

	//Ruta del archivo JSON

	public static final String JSON = "./data/bikeRoutes.JSON";
	public static final String COMIENZOA = "map.on(\"load\", function() {\r\n" + "    map.addSource(\"intersecciones\", {\r\n" + "        \"type\": \"geojson\",\r\n" + "        \"data\":{\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [ ";
	public final static String BEGIN = "<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "    <meta charset='utf-8' />\r\n" + "    <title>Mapa de Chicago</title>\r\n" + "    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />\r\n" + "    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.js'></script>\r\n" + "    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.css' rel='stylesheet' />\r\n" + "    <style>\r\n" + "        body { margin:0; padding:0; }\r\n" + "        #map { position:absolute; top:0; bottom:0; width:100%; }\r\n" + "    </style>\r\n" + "</head>\r\n" + "<body>\r\n" + "\r\n" + "<div id=\"map\"></div>\r\n" + "<script>\r\n" + "mapboxgl.accessToken = 'pk.eyJ1Ijoic29maWFhbHZhcmV6IiwiYSI6ImNqb2RrY25kMTB5MnYzcG80ZWp4ZWltNWoifQ.nRV5ZWhw1frGZQqx2BozeQ';\r\n" + "var map = new mapboxgl.Map({\r\n" + "    container: \"map\",\r\n" + "    style: \"mapbox://styles/mapbox/outdoors-v9\",\r\n" + "    center: [-87.65, 41.84],\r\n" + "    zoom: 11\r\n" + "});";
	public final static String END = "</script>\r\n" + "\r\n" + "</body>\r\n" + "</html>";
	private int numberOfTrips;
	private int numberOfStations;
	private int numberOfBikes;
	private WeightedGraph grafo2;
	private Digraph grafoDirigidoC1;
	private LinearProbing<Integer, VOStation> stationHash;
	private DoublyLinkedList<VOStation> stationList;
	private RedBlackBST<Integer, VOTrip> totalTripsTree;
	private RedBlackBST<Integer, VOBike> bikes;
	private DoublyLinkedList<VOStation> stCongest; 


	private int longitudes = 10;
	private double longitudMaxima = -1000000;
	private double latitudMaxima = -1000000;
	private double longitudMinima = 1000000;
	private int latitudes = 10;

	private Graph<Integer,VOPoint,VOEdge> grafo;

	public int nodosmixtos = 0;

	private double latitudMinima = 1000000;
	private DoublyLinkedList<VOStation> congestionadas;
	private LinearProbing <String, VOTripsEntreEstaciones> elHashDeLosViajes;
	DoublyLinkedList<VOConnectedComponent> lista;

	private int numeroIntersecciones;
	public int getNumeroIntersecciones() {
		return numeroIntersecciones;
	}

	public void setNumeroIntersecciones(int numeroIntersecciones) {
		this.numeroIntersecciones = numeroIntersecciones;
	}

	//Para las estaciones ponemos el boolean de vopoint en true y sus ids seran iguales a sus id + numero de estaciones 
	// Los que tengan id < 321375 seran intersecciones y los que tengan mayor seran estaciones


	public int getNumberOfTrips() {
		return numberOfTrips;
	}

	public int getNumberOfBikes()
	{
		return numberOfBikes;
	}

	public void setNumberOfTrips(int numberOfTrips) {
		this.numberOfTrips = numberOfTrips;
	}

	public int getNumberOfStations() {
		return numberOfStations;
	}

	public void setNumberOfStations(int numberOfStations) {
		this.numberOfStations = numberOfStations;
	}


	@Override
	public void cargarDatos(String rutaTrips, String rutaStations) {
		String[] trips = rutaTrips.split(":");
		String[] stations = rutaStations.split(":");
		for(String j : stations)
		{
			loadStations(j);
		}
		for(String i : trips)
		{
			loadTrips(i);
		}

		System.out.println("Cantidad de trips: "+totalTripsTree.size());
	}

	public void loadTrips (String tripsFile)
	{
		elHashDeLosViajes = new LinearProbing<>(1000);
		totalTripsTree = new RedBlackBST<>();
		loadTree(tripsFile);

	}

	public void loadEdges(String ruta) throws Exception
	{
		try{
			FileReader fr = new FileReader(ruta);
			BufferedReader br = new BufferedReader(fr);
			String s = br.readLine();
			LinearProbing<Integer, VOPoint> nodos = grafo.getV();
			while (s!=null)
			{
				String[] arr = s.split(" ");
				if(arr.length != 1)
				{
					int id = Integer.parseInt(arr[0]);
					VOPoint p = nodos.get(id);
					double longp = p.getLongitude();
					double latp = p.getLatitude();
					for(int i = 1; i < arr.length; i++)
					{

						int otroid = Integer.parseInt(arr[i]);
						VOPoint otrop = nodos.get(otroid);
						if(otrop!= null)
						{
							double longotrop = otrop.getLongitude();
							double latotrop = otrop.getLatitude();
							double distancia = calculateDistance(latp, longp, latotrop, longotrop);
							VOEdge e = new VOEdge(p, otrop, distancia);
							grafo.addEdge(id, otroid, e);
							grafo2.addArc(new Arco(id, otroid, distancia));
						}
					}
				}
				s = br.readLine();
			}
			br.close();
			fr.close();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	private void loadTree(String route)
	{
		try
		{
			FileReader fr =  new FileReader(route);
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			String data = br.readLine();
			while(data != null)
			{
				data = data.replace('"', ' ');
				String[] datos = data.split(",");
				int trip_id = Integer.parseInt(datos[0].trim());
				String start_time = datos[1].trim();
				String end_time = datos[2].trim();
				int bikeid = Integer.parseInt(datos[3].trim());
				int tripduration = Integer.parseInt(datos[4].trim());
				int from_station_id = Integer.parseInt(datos[5].trim());
				String from_station_name = datos[6].trim();
				int to_station_id = Integer.parseInt(datos[7].trim());
				String to_station_name = datos[8].trim();
				String usertype = datos[9].trim();
				String gender;
				String birthyear;

				if(datos.length == 10)
				{
					gender = VOTrip.UNKNOWN;
					birthyear = "";
				}
				else if(datos.length == 11)
				{
					gender = datos[10].trim();
					birthyear ="";
				}
				else
				{
					gender = datos[10].trim();
					birthyear = datos[11].trim();
				}
				VOStation inicio = buscarEstacion(from_station_id);
				VOStation fin = buscarEstacion(to_station_id);
				aumentarCongestionS(from_station_id);
				aumentarCongestionL(to_station_id);
				double dist = calculateDistance(inicio.getLatitude(), inicio.getLongitude(), fin.getLatitude(), fin.getLongitude());
				VOTrip todo = new VOTrip(trip_id, start_time, end_time, bikeid, tripduration, from_station_id, from_station_name, to_station_id, to_station_name, usertype, gender, birthyear, dist);				
				totalTripsTree.put(trip_id, todo);
				numberOfTrips++;
				VOTripsEntreEstaciones t = elHashDeLosViajes.get(from_station_id + "/" + to_station_id);
				if(t == null)
				{
					t = new VOTripsEntreEstaciones(inicio, fin);
					t.aumentarNumViajes();
					t.aumentarTiempoTotal(tripduration);
					t.genero(gender);
					elHashDeLosViajes.put(from_station_id + "/" + to_station_id, t);
				}
				else
				{
					t.aumentarNumViajes();
					t.aumentarTiempoTotal(tripduration);
					t.genero(gender);
					elHashDeLosViajes.put(from_station_id + "/" + to_station_id, t);
				}

				data = br.readLine();

			}
			br.close();
			fr.close();
		}
		catch(Exception e)
		{
			System.out.println("hubo una excepcion cargando los viajes");
			e.printStackTrace();
		}
	}

	public void loadPoints(String ruta) throws Exception
	{
		try{
			grafo = new Graph<>();

			FileReader fr = new FileReader(ruta);
			BufferedReader br = new BufferedReader(fr);
			String s = br.readLine();
			int x = 0;
			while (s!=null)
			{
				String[] arr = s.split(",");
				int id = Integer.parseInt(arr[0]);
				double lat = Double.parseDouble(arr[2]);
				double lon = Double.parseDouble(arr[1]);
				VOPoint p = new VOPoint(id, lon, lat, false);
				grafo.addVertex(id, p);
				numeroIntersecciones++;
				s = br.readLine();
				x++;
			}
			grafo2 = new  WeightedGraph(x + numberOfStations);
			System.out.println( "Numero de intersecciones: " + numeroIntersecciones);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public void loadStations (String stationsFile) {
		if(stationHash == null)
		{
			stationHash = new LinearProbing<Integer, VOStation>(700);
		}
		try
		{
			FileReader fr =  new FileReader(stationsFile);
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			String data = br.readLine();
			stationList = new DoublyLinkedList<>();
			while(data != null)
			{
				data = data.replace('"', ' ');
				String[] datos = data.split(",");
				int id = Integer.parseInt(datos[0].trim());
				String name = datos[1].trim();
				String city =  datos[2].trim();
				double latitude =  Double.parseDouble(datos[3].trim());
				double longitude =  Double.parseDouble(datos[4].trim());
				int dpcapacity =  Integer.parseInt(datos[5].trim());
				String online_LocalDateTime =  datos[6].trim();
				data = br.readLine();
				stationHash.put(id, new VOStation(id, name, city, latitude, longitude, dpcapacity, online_LocalDateTime));
				stationList.addAtBeginning(new VOStation(id, name, city, latitude, longitude, dpcapacity, online_LocalDateTime));
				if(longitude > longitudMaxima)
				{
					longitudMaxima = longitude;
				}
				if(longitude < longitudMinima)
				{
					longitudMinima = longitude;
				}
				if(latitude > latitudMaxima)
				{
					latitudMaxima = latitude;
				}
				if(latitude < latitudMinima)
				{
					latitudMinima = latitude;
				}
				numberOfStations++;
			}
			br.close();
			fr.close();
		}
		catch(Exception e)
		{
			System.out.println("hubo una excepcion cargando las estaciones");
			System.out.println(e.getMessage());
		}

	}

	public void cargarJSON() throws Exception{

		try{
			Object obj = new JSONParser().parse(new FileReader ("data/mallaVial.JSON"));
			org.json.JSONObject jo =  new org.json.JSONObject(obj.toString()); 
			grafo = new Graph<>();
			org.json.JSONArray puntos = (org.json.JSONArray) jo.get("puntos");
			for (int i = 0; i < puntos.length() - 1; i++) {
				org.json.JSONObject p = (org.json.JSONObject) puntos.getJSONObject(i);
				int id = p.getInt("id");
				Double longitud = Double.parseDouble(p.getString("longitud"));
				Double latitud = Double.parseDouble(p.getString("latitud"));
				boolean estacion = p.getBoolean("estacion");
				if(!estacion){
					numeroIntersecciones++;
				}

				VOPoint puntito = new VOPoint(id, longitud,latitud, estacion);
				grafo.addVertex(id, puntito);

			}
			grafo2 = new WeightedGraph(322003);
			org.json.JSONArray edges = (org.json.JSONArray) jo.get("edges");
			for (int i = 0; i < edges.length() - 1; i++) {
				org.json.JSONObject e = edges.getJSONObject(i);
				int id1 = e.getInt("id1");
				int id2 = e.getInt("id2");
				Double peso = Double.parseDouble(e.getString("peso"));
				LinearProbing<Integer, VOPoint> lp = grafo.getV();
				VOPoint p1 = lp.get(id1);
				VOPoint p2 = lp.get(id2);
				if(p2.isEstacion()){
					int idestacion = id2 - numeroIntersecciones;
					VOStation st = stationHash.get(idestacion);
					VOEdge edgy = new VOEdge(p1, st, peso);
					grafo.addEdge(id1, id2, edgy);
					grafo2.addArc(new Arco(id1, id2, peso) );

				}
				else{
					VOEdge edgy = new VOEdge(p1, p2, peso);
					grafo.addEdge(id1, id2, edgy);
					grafo2.addArc(new Arco(id1, id2, peso));
				}


			}
			System.out.println("Cantidad vertices: " + grafo.V());
			System.out.println("Cantidad edges: " + grafo.E());



		}
		catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public void aumentarCongestionS(int id)
	{
		Iterator<Integer> it = stationHash.keys();
		VOStation st = null;
		while(it.hasNext()){
			if(it.next() == id){
				st = stationHash.get(id);
				st.cSalidaMas();
				break;
			}
		}
	}


	public void aumentarCongestionL(int id){
		Iterator<Integer> it = stationHash.keys();
		VOStation st = null;
		while(it.hasNext()){
			if(it.next() == id){
				st = stationHash.get(id);
				st.cLlegadaMas();
				break;
			}
		}
	}

	public void cargarNodosEstaciones() {
		if(grafo == null)
		{
			grafo = new Graph<>();
		}
		Iterator <Integer> iter = stationHash.keys();
		while(iter.hasNext())
		{
			Integer i = iter.next();
			VOStation st = stationHash.get(i);
			double latitud = st.getLatitude();
			double longitud = st.getLongitude();
			VOPoint p = new VOPoint(i + numeroIntersecciones, longitud, latitud, true);
			grafo.addVertex(i + numeroIntersecciones, p);
			LinearProbing<Integer, VOPoint> nodos = grafo.getV();
			Iterator<Integer> it = nodos.keys();
			double dist = Double.MAX_VALUE;
			VOPoint puntito = null;
			while(it.hasNext())
			{
				VOPoint point = nodos.get(it.next());
				if(!point.isEstacion())
				{
					double d = calculateDistance(st.getLatitude(), st.getLongitude(), point.getLatitude(), point.getLongitude());
					if(dist > d)
					{
						dist = d;
						puntito = point;
					}
				}
			}
			VOEdge arc = new VOEdge(puntito, p, dist);
			grafo.addEdge(puntito.getId(),i + numeroIntersecciones,  arc);
			nodosmixtos++;
		}
		System.out.println("Vertices totales: " + grafo.V());
		System.out.println("Edges totales: " + grafo.E());
		System.out.println("Enlaces mixtos: " + nodosmixtos);
		// TODO Auto-generated method stub

	}






	public double calculateDistance(double startLat, double startLong,
			double endLat, double endLong)
	{
		final int earthRadius = 6371;

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return earthRadius * c; // <-- d
	}

	private static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}

	private VOStation buscarEstacion(int id)
	{

		VOStation element = null;
		if (stationList != null)
		{
			boolean found = false;
			if(stationList.getElement(0).getId() == id)
			{
				found = true;
				element = stationList.getElement(0);
			}
			else
			{
				Iterator<VOStation> i = stationList.iterator();
				while(i.hasNext() && !found)
				{
					VOStation b = i.next();
					if(b.getId() == id)
					{
						found = true;
						element = b;
					}

				}
			}

		}
		return element;
	}

	private VOPoint buscarPunto(int id)
	{
		VOPoint element = null;
		boolean found = false;
		LinearProbing<Integer, VOPoint> nodos = grafo.getV();
		Iterator<Integer> iter = nodos.keys();
		while(iter.hasNext() && !found)
		{
			if(iter.next() == id)
			{
				element = nodos.get(id);
				found = true;
			}
		}
		return element;
	}

	public void hacerhtml() {
		try {
			PrintWriter fr = new PrintWriter("data/mapa.html");
			fr.println(BEGIN);
			fr.println("map.on(\"load\", function() {\r\n" + "    map.addSource(\"intersecciones\", {\r\n" + "        \"type\": \"geojson\",\r\n" + "        \"data\":{\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [ ");
			LinearProbing<Integer, VOPoint> puntos = grafo.getV();
			Iterator<Integer> iter = puntos.keys();
			while(iter.hasNext()){
				int number = iter.next();
				VOPoint p = puntos.get(number);
				double longitud = p.getLongitude();
				double latitud = p.getLatitude();
				if(!p.isEstacion())
					fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: ["+ longitud + ", " + latitud + "]}},\r\n");
			}
			fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: [0, 0]}}\r\n");
			fr.println("]}});");
			fr.println("    map.addLayer({\r\n" + "        \"id\": \"inter\",\r\n" + "        \"type\": \"circle\",\r\n" + "        \"source\": \"intersecciones\",\r\n" + "        \"paint\": {\r\n" + "            \"circle-radius\": 3,\r\n" + "            \"circle-color\": \"#008000\"\r\n" + "        },\r\n" + "    });");
			fr.println("map.addSource(\"estaciones\", {\r\n" + "        \"type\": \"geojson\",\r\n" + "        \"data\":{\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [ ");
			puntos = grafo.getV();
			iter = puntos.keys();
			while(iter.hasNext()){
				int number = iter.next();
				VOPoint p = puntos.get(number);
				double longitud = p.getLongitude();
				double latitud = p.getLatitude();
				if(p.isEstacion())
					fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: ["+ longitud + ", " + latitud + "]}},\r\n");
			}
			fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: [0, 0]}}\r\n");
			fr.println("]}});");
			fr.println(AStringAgregarCapa("stations", "circle", "estaciones")+"        \"paint\": {\r\n" + "            \"circle-radius\": 3,\r\n" + "            \"circle-color\": \"#ff0000\"\r\n" + "        },\r\n" + "    });");
			fr.println(AStringAgregarCapa("route", "line", "{")+"            \"type\": \"geojson\",\r\n" + "            \"data\": {\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [");
			LinearProbing <String, VOEdge> adj = grafo.getAdj();
			Iterator< String> iterador = adj.keys();
			while(iterador.hasNext()){
				String s = iterador.next();
				VOEdge e = adj.get(s);
				if(e!=null){
					double lonInicial = e.getPunto1().getLongitude();
					double latInicial = e.getPunto1().getLatitude();
					double lonFinal;
					double latFinal;
					if(e.getPunto2() == null){
						lonFinal = e.getRelacionEstacion().getLongitude();
						latFinal = e.getRelacionEstacion().getLatitude();
					}else{
						lonFinal = e.getPunto2().getLongitude();
						latFinal = e.getPunto2().getLatitude();
					}
					fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [["+lonInicial+","+latInicial+"],["+lonFinal+","+latFinal+"]]}},\r\n" + "");
				}
			}
			fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [[0,0],[0,1]]}}  ]}        },");
			fr.println("\"layout\": {\r\n" + "            \"line-join\": \"round\",\r\n" + "            \"line-cap\": \"round\"\r\n" + "        },\r\n" + "        \"paint\": {\r\n" + "            \"line-color\": \"#000000\",\r\n" + "            \"line-width\": 2\r\n" + "        }\r\n" + "    });");
			fr.println("});" + END);
			fr.close();
			File f = new File ("data/mapa.html");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	public void crearJSON() throws Exception{
		try {

			LinearProbing<Integer, VOPoint> puntitos = grafo.getV();
			Iterator <Integer> iter = puntitos.keys();
			File archivo = new File("data/mallaVial.JSON");
			PrintWriter pWriter = new PrintWriter(archivo);
			pWriter.println("{" + '\n'+ ' ');
			pWriter.println(" " + "\"puntos\":[" + '\n');
			while(iter.hasNext()){
				int x = iter.next();
				VOPoint p = puntitos.get(x);
				String datos = escribirJSON(p.getId(),p.getLongitude(),p.getLatitude(),p.isEstacion(),1);
				pWriter.println(datos + '\n');
			}
			String datosf = escribirJSON(-1,0,0,false,1);
			pWriter.println(datosf + '\n');
			pWriter.println("] ");
			pWriter.println("," + '\n' + ' ');
			LinearProbing<String, VOEdge> adj = grafo.getAdj();
			Iterator <String> it = adj.keys();
			pWriter.println( " " + "\"edges\":[" + '\n');
			while(it.hasNext()){
				String x = it.next();
				VOEdge e = adj.get(x);
				int v2;
				if(e.getPunto2() == null){
					v2 = e.getRelacionEstacion().getId();
				}else{
					v2 = e.getPunto2().getId();
				}
				String datos = escribirJSON( e.getPunto1().getId()+0.0, v2+0.0, e.getPeso(), true, 2);
				pWriter.println( datos + '\n');
			}
			String finalDatos =  escribirJSON(-1,0,0,true,2);
			pWriter.println( finalDatos + '\n');
			pWriter.println("] ");
			String todo = '\n'+ "}";
			pWriter.println(todo);
			pWriter.close();
		}
		catch (Exception e){
			System.out.println(e.getMessage());
		}

	}

	private String escribirJSON(double p1, double p2, double p3, boolean p4, int num){
		if(num==1)
			return "  " + "{"+ '\n'+ "   " + "\"id\": \"" + p1 + "\", " + '\n'+ "   " + "\"longitud\": \"" + p2 + "\"," + '\n'+ "   " + "\"latitud\": \"" + p3 + "\", " + '\n'+ "   " + "\"estacion\": " + p4 + ", " + '\n'				+ "  " + "},";
		else if(num==2)
			return "  " + "{"+ '\n'+ "   " + "\"id1\": \"" + p1 + "\", " + '\n'+ "   " + "\"id2\": \"" + p2 + "\"," + '\n'+ "   " + "\"peso\": \"" + p3 + "\", " + '\n'			+ "  " + "},";
		return "";
	}

	@Override
	public VOPath<VOPoint> A1_menorDistancia(double latInicial, double lonInicial, double latFinal, double lonFinal) {			// TODO Auto-generated method stub
		VOPath<VOPoint> elPath = new VOPath<VOPoint>();
		IDoublyLinkedList<VOPoint> recorrido = new DoublyLinkedList<>();
		Iterator<Integer> iter = stationHash.keys();


		//buscar dsitancia y estacion final e inicial
		double dInicial = Double.MAX_VALUE;
		double dFinal = Double.MAX_VALUE;
		VOStation estacionInicial = null;
		VOStation estacionFinal = null;
		while(iter.hasNext())
		{
			VOStation st = stationHash.get(iter.next());
			double diestacionInicial = calculateDistance(st.getLatitude(), st.getLongitude(), latInicial, lonInicial);
			double diestacionFinal = calculateDistance(st.getLatitude(), st.getLongitude(), latFinal, lonFinal);
			if(dInicial > diestacionInicial){
				dInicial = diestacionInicial;
				estacionInicial = st;
			}
			if(dFinal > diestacionFinal){
				dFinal = diestacionFinal;
				estacionFinal = st;
			}
		}

		//hacer Dijkstra para encontrar la menor distancia
		Dijkstra dijkstra = new Dijkstra(grafo2, estacionInicial.getId() + numeroIntersecciones);
		Iterable<Arco> it = dijkstra.pathTo(estacionFinal.getId() + numeroIntersecciones);
		LinearProbing <Integer, VOPoint> tabla = grafo.getV();
		VOPoint inicial = new VOPoint(0, lonInicial, latInicial, false);
		recorrido.addAtBeginning(inicial);
		VOPoint anterior = inicial;
		//va agregando los nodos al path y calculando distancia
		for(Arco arc : it)
		{

			VOPoint p = tabla.get(arc.either());
			recorrido.addAtEnd(p);
			elPath.addDist(calculateDistance(p.getLatitude(), p.getLongitude(), anterior.getLatitude(), anterior.getLongitude()));
			anterior = p;
		}
		VOPoint elFinal = new VOPoint(0, lonFinal, latFinal, false);
		elPath.addDist(calculateDistance(anterior.getLatitude(), anterior.getLongitude(), elFinal.getLatitude(), elFinal.getLongitude()));
		recorrido.addAtEnd(elFinal);
		//html
		try {
			PrintWriter fr = new PrintWriter("data/A1.html");
			fr.println(BEGIN);
			fr.println("map.on(\"load\", function() {\r\n" + "    map.addSource(\"intersecciones\", {\r\n" + "        \"type\": \"geojson\",\r\n" + "        \"data\":{\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [ ");
			for (int i = 0; i < recorrido.getSize(); i++) {
				fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: ["+ recorrido.getElement(i).getLongitude() + ", " + recorrido.getElement(i).getLatitude() + "]}},\r\n");
				System.out.println(recorrido.getElement(i).getLongitude() + ", " + recorrido.getElement(i).getLatitude() );
			}
			fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: [0, 0]}}\r\n");
			fr.println("]}});");
			fr.println("    map.addLayer({\r\n" + "        \"id\": \"inter\",\r\n" + "        \"type\": \"circle\",\r\n" + "        \"source\": \"intersecciones\",\r\n" + 	"        \"paint\": {\r\n" + "            \"circle-radius\": 3,\r\n" + "            \"circle-color\": \"#008000\"\r\n" + "        },\r\n" + "    });");
			fr.println("map.addLayer({\r\n" + "        \"id\": \"route\",\r\n" + "        \"type\": \"line\",\r\n" + "        \"source\": {\r\n" + "            \"type\": \"geojson\",\r\n" + "            \"data\": {\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [");
			for (int i = 0; i < recorrido.getSize() - 1; i++) {
				fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [["+recorrido.getElement(i).getLongitude()+","+recorrido.getElement(i).getLatitude()+"],["+recorrido.getElement(i + 1).getLongitude()+","+recorrido.getElement(i + 1).getLatitude()+"]]}},\r\n" + "");
			}

			fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [[0,0],[0,1]]}}  ]}        },");
			fr.println("\"layout\": {\r\n" + "            \"line-join\": \"round\",\r\n" + "            \"line-cap\": \"round\"\r\n" + "        },\r\n" + "        \"paint\": {\r\n" + "            \"line-color\": \"#000000\",\r\n" + "            \"line-width\": 2\r\n" + "        }\r\n" + "    });");
			fr.println("});" + END);
			fr.close();
			File f = new File ("data/A1.html");


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		elPath.addListaNodos(recorrido);
		System.out.println("Distancia estimada del camino :  " + elPath.getTotalDistance());
		System.out.println("Estacion mas cercana a origen :  "+ estacionInicial.getName());
		System.out.println("Estaciones mas cercana a destino :  "+estacionFinal.getName());
		return elPath;
	}

	private String AStringCadaPunto(VOPoint v, VOPoint v1, int num){
		if(num==1)
			return "{type: 'Feature', geometry: { type: 'Point', coordinates: ["+ v.getLongitude() + ", " + v.getLatitude() + "]}},\r\n";
		else if(num==2)
			return"{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [["+v.getLongitude()+","+v.getLatitude()+"],["+v1.getLongitude()+","+v1.getLatitude()+"]]}},\r\n" + "";
		return "";
	}

	private String AStringAgregarCapa(String p1, String p2, String p3){
		return "    map.addLayer({\r\n" + "        \"id\": \""+p1+"\",\r\n" + "        \"type\": \""+p2+"\",\r\n" + "        \"source\": "+p3+"\r\n";	       
	}

	@Override
	public VOPath<VOPoint> A2_menorNumVertices(double latInicial, double lonInicial, double latFinal, double lonFinal) {
		Iterator<Integer> iter = stationHash.keys();
		VOPath<VOPoint> elCamino = new VOPath<VOPoint>();
		double dInicial = Double.MAX_VALUE;
		double dFinal = Double.MAX_VALUE;
		VOStation estacionInicial = null;
		VOStation estacionFinal = null;
		//buscar dsitancia y estacion final e inicial
		while(iter.hasNext()){
			VOStation st = stationHash.get(iter.next());
			double inicialDist = calculateDistance(st.getLatitude(), st.getLongitude(), latInicial, lonInicial);
			double finalDist = calculateDistance(st.getLatitude(), st.getLongitude(), latFinal, lonFinal);
			if(dFinal > finalDist){
				dFinal = finalDist;
				estacionFinal = st;
			}
			if(dInicial > inicialDist){
				dInicial = inicialDist;
				estacionInicial = st;
			}
		}

		//hacer bfs para encontrar el menornumero de vertices
		BFS bfs = new BFS(grafo2, estacionInicial.getId() + numeroIntersecciones);
		Iterable<Integer> iter2 = bfs.pathTo(estacionFinal.getId() + numeroIntersecciones);
		VOPoint inicial = new VOPoint(0, lonInicial, latInicial, false);
		elCamino.addNodoInicio(inicial);
		VOPoint anterior = inicial;

		//va agregando los nodos al path y calculando distancia
		for(Integer i : iter2){
			VOPoint p = buscarPunto(i);
			elCamino.addNodoFinal(p);
			elCamino.addDist(calculateDistance(anterior.getLatitude(), anterior.getLongitude(), p.getLatitude(), p.getLongitude()));
			anterior = p;
		}
		VOPoint ultimo = new VOPoint(0, lonFinal, latFinal, false);
		elCamino.addNodoFinal(ultimo);
		IDoublyLinkedList<VOPoint> laLista = elCamino.darListaNodos();
		//escribe los puntos en el html
		try {
			PrintWriter fr = new PrintWriter("data/A2.html");
			fr.println(BEGIN);
			fr.println(COMIENZOA);
			for (int i = 0; i < laLista.getSize(); i++){
				System.out.println(laLista.getElement(i).getLongitude() + ", " + laLista.getElement(i).getLatitude());
				fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: ["+ laLista.getElement(i).getLongitude() + ", " + laLista.getElement(i).getLatitude() + "]}},\r\n");
			}
			fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: [0, 0]}}\r\n");
			fr.println("]}});");
			fr.println(AStringAgregarCapa("inter","circle","\"intersecciones\",")+"        \"paint\": {\r\n" + "            \"circle-radius\": 3,\r\n" + "            \"circle-color\": \"#008000\"\r\n" + "        },\r\n" + "    });");
			fr.println(AStringAgregarCapa("route","line", "{")+"            \"type\": \"geojson\",\r\n" + "            \"data\": {\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [");
			for (int i = 0; i < laLista.getSize() - 1; i++) {
				fr.println(AStringCadaPunto(laLista.getElement(i),laLista.getElement(i+1),2));
			}
			fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [[0,0],[0,1]]}}  ]}        },");
			fr.println("\"layout\": {\r\n" + "            \"line-join\": \"round\",\r\n" + "            \"line-cap\": \"round\"\r\n" + "        },\r\n" + "        \"paint\": {\r\n" + "            \"line-color\": \"#000000\",\r\n" + "            \"line-width\": 2\r\n" + "        }\r\n" + "    });");
			fr.println("});" + END);
			fr.close();
			System.out.println("Distancia estimada del camino :  "+elCamino.getTotalDistance());
			System.out.println("Estacion mas cercana a origen :  "+estacionInicial.getName());
			System.out.println("Estaciones mas cercana a destino :  "+estacionFinal.getName());
			File f = new File ("data/A2.html");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return elCamino;

	}




	public DoublyLinkedList<VOStation> B1_estacionesCongestionadas(int n) {
		//TODO Visualizacion Mapa
		stCongest = new DoublyLinkedList<VOStation>();

		MaxHeap<Integer> congestion = new MaxHeap<>(n);

		Iterator<Integer> iter1 = stationHash.keys();
		while(iter1.hasNext()) {
			int viajesTotales = stationHash.get(iter1.next()).getViajesLlegan() +
					stationHash.get(iter1.next()).getViajesSalen();
			//Ordena por congestión de mayor a menor
			congestion.insert(viajesTotales);
		}

		Iterator<Integer> iter = stationHash.keys();
		while (iter.hasNext() && stCongest.getSize() <=n) {
			VOStation st = stationHash.get(iter.next());
			int viajesTotales =st.getViajesLlegan() +
					st.getViajesSalen();
			if(viajesTotales == congestion.delMax()) {
				stCongest.addAtEnd(stationHash.get(iter.next()));
				VOPoint s = new VOPoint(stationHash.get(iter.next()).getId(),stationHash.get(iter.next()).getLatitude(), stationHash.get(iter.next()).getLongitude(),
						true);

			}



			iter.next();
		}
		//DONE imprimir en consola 

		//DONE Pintar mapa 
		try {
			PrintWriter fr = new PrintWriter("data/B1.html");
			fr.println(BEGIN);
			fr.println(COMIENZOA);
			for (int i = 0; i < stCongest.getSize(); i++){
				fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: ["+ stCongest.getElement(i).getLongitude() + ", " + stCongest.getElement(i).getLatitude() + "]}},\r\n");
			}
			fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: [0, 0]}}\r\n");
			fr.println("]}});");
			fr.println(AStringAgregarCapa("inter","circle","\"intersecciones\",")+"        \"paint\": {\r\n" + "            \"circle-radius\": 3,\r\n" + "            \"circle-color\": \"#008000\"\r\n" + "        },\r\n" + "    });");
			fr.println(AStringAgregarCapa("route","line", "{")+"            \"type\": \"geojson\",\r\n" + "            \"data\": {\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [");
			for (int i = 0; i < stCongest.getSize(); i++) {
				fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [["+stCongest.getElement(0).getLongitude()+","+stCongest.getElement(0).getLatitude()+"],["+stCongest.getElement(i).getLongitude()+","+stCongest.getElement(i).getLatitude()+"]]}},\r\n" + "");
			}	
			fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [[0,0],[0,1]]}}  ]}        },");
			fr.println("\"layout\": {\r\n" + "            \"line-join\": \"round\",\r\n" + "            \"line-cap\": \"round\"\r\n" + "        },\r\n" + "        \"paint\": {\r\n" + "            \"line-color\": \"#000000\",\r\n" + "            \"line-width\": 2\r\n" + "        }\r\n" + "    });");
			fr.println("});" + END);
			fr.close();
			File f = new File ("data/B1.html");
		} catch (IOException e) {
			e.printStackTrace();
		}


		return stCongest;
	}

	public IDoublyLinkedList<VOPath> B2_rutasMinimas(DoublyLinkedList<VOStation> stations) {


		//Usamos la lista del punto anterior para buscar los caminos entre esas estaciones en el grafo
		//Rutas mínimas = MST 
		stations = stCongest;
		IDoublyLinkedList<VOPath> req = new DoublyLinkedList<VOPath>();
		WeightedGraph estaciones = new WeightedGraph(100000);
		VOPath caminos = new VOPath<>();

		caminos.addNodoInicio(stations.getElement(0));
		VOPoint inicial = new VOPoint(stations.getElement(0).getId(), stations.getElement(0).getLongitude(), stations.getElement(0).getLatitude(), true);
		VOPoint anterior = inicial;

		for (int i = 1; i < stations.getSize(); i++) {
			VOPoint p = new VOPoint(stations.getElement(i).getId(), stations.getElement(i).getLongitude(), stations.getElement(i).getLatitude(), true);
			double distancia = calculateDistance(anterior.getLatitude(), anterior.getLongitude(), p.getLatitude(), p.getLongitude());
			caminos.addNodoFinal(stations.getElement(i));
			caminos.addDist(distancia);
			VOEdge a = new VOEdge(anterior, p, distancia);  //NOTA: Hacer que de alguna manera los arcos se comparen por distancia minima y se escoja ese como MST 
			System.out.println(estaciones.adj1(a));

			estaciones.addEdge(a);
			anterior = p;
		}

		//Se busca el MST del grafo generado 

		PrimMST prim = new PrimMST(estaciones); //El peso de prim será el valor de rutas mínimas
		req= caminos.darListaNodos();		

		System.out.println("Distancia total: " + prim.weight()); 

		Iterator<VOEdge> it = prim.iterator1();
		VOEdge arco =it.next();


		//DONE Visualizacion Mapa
		try {
			PrintWriter fr = new PrintWriter("data/B2.html");
			fr.println(BEGIN);
			fr.println(COMIENZOA);
			while(it.hasNext()){
				fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: ["+ arco.getPunto1().getLongitude() + ", " + arco.getPunto1().getLatitude() + "]}},\r\n");
				arco=it.next();
			}
			fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: [0, 0]}}\r\n");
			fr.println("]}});");
			fr.println(AStringAgregarCapa("inter","circle","\"intersecciones\",")+"        \"paint\": {\r\n" + "            \"circle-radius\": 3,\r\n" + "            \"circle-color\": \"#008000\"\r\n" + "        },\r\n" + "    });");
			fr.println(AStringAgregarCapa("route","line", "{")+"            \"type\": \"geojson\",\r\n" + "            \"data\": {\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [");
			while(it.hasNext()) {
				fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [["+arco.getPunto1().getLongitude()+","+arco.getPunto1().getLatitude()+"],["+arco.getPunto2().getLongitude()+","+arco.getPunto2().getLongitude()+"]]}},\r\n" + "");
				arco=it.next();
			}	
			fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [[0,0],[0,1]]}}  ]}        },");
			fr.println("\"layout\": {\r\n" + "            \"line-join\": \"round\",\r\n" + "            \"line-cap\": \"round\"\r\n" + "        },\r\n" + "        \"paint\": {\r\n" + "            \"line-color\": \"#000000\",\r\n" + "            \"line-width\": 2\r\n" + "        }\r\n" + "    });");
			fr.println("});" + END);
			fr.close();
			File f = new File ("data/B2.html");
		} catch (IOException e) {
			e.printStackTrace();
		}




		return req;
	}

	public Digraph C1_grafoEstaciones() {
		WeightedDigraph grafoConPesos = new WeightedDigraph(9999999);
		grafoDirigidoC1 = new Digraph(9999999);
		Iterator <String> iter = elHashDeLosViajes.keys();
		double mayorPromedio = 0;
		int cant = 0;
		//agregar los arcos y los vertices y va buscando el mayor promedio de tiempo de viajes
		while(iter.hasNext()){
			String s = iter.next();
			VOTripsEntreEstaciones t = elHashDeLosViajes.get(s);
			grafoConPesos.addEdge(new ArcoDirigido(t.darEstacionInicio().getId(), t.darEstacionFin().getId(), t.darpromedio()));
			grafoDirigidoC1.addEdge(t.darEstacionInicio().getId(), t.darEstacionFin().getId());
			if(t.darpromedio() > mayorPromedio){
				mayorPromedio = t.darpromedio();
			}	
			cant++;
		}
		System.out.println("Numero de vertices : "+grafoConPesos.E());
		System.out.println("Numero de arcos : "+cant);
		//dibujo html
		try {
			PrintWriter fr = new PrintWriter("data/C1.html");
			fr.println(BEGIN);
			fr.println("map.on(\"load\", function() {\r\n" + "    map.addSource(\"estaciones\", {\r\n" + "        \"type\": \"geojson\",\r\n" + "        \"data\":{\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [ ");

			for (VOStation st : stationList) {
				fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: ["+ st.getLongitude() + ", " + st.getLatitude() + "]}},\r\n");
			}

			fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: [0, 0]}}\r\n");
			fr.println("]}});");
			fr.println("    map.addLayer({\r\n" + "        \"id\": \"inter\",\r\n" + "        \"type\": \"circle\",\r\n" + "        \"source\": \"estaciones\",\r\n" + "        \"paint\": {\r\n" + "            \"circle-radius\": 5,\r\n" + "            \"circle-color\": \"#123456\"\r\n" + "        },\r\n" + "    });");
			fr.println("map.addLayer({\r\n" + "        \"id\": \"route\",\r\n" + "        \"type\": \"line\",\r\n" + "        \"source\": {\r\n" + "            \"type\": \"geojson\",\r\n" + "            \"data\": {\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [");

			iter = elHashDeLosViajes.keys();
			while(iter.hasNext()){
				String s = iter.next();
				VOTripsEntreEstaciones t = elHashDeLosViajes.get(s);
				fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [["+t.darEstacionInicio().getLongitude()+","+t.darEstacionInicio().getLatitude()+"],["+t.darEstacionFin().getLongitude()+","+ t.darEstacionFin().getLatitude()+"]]}},\r\n" );				

			}

			fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [[0,0],[0,1]]}}  ]}        },");
			fr.println("\"layout\": {\r\n" + "            \"line-join\": \"round\",\r\n" + "            \"line-cap\": \"round\"\r\n" + "        },\r\n" + "        \"paint\": {\r\n" + "            \"line-color\": \"#000000\",\r\n" + "            \"line-width\": 2\r\n" + "        }\r\n" + "    });");

			fr.println("});" + END);
			fr.close();
			File f = new File ("data/C1.html");


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return grafoDirigidoC1;
	}



	public DoublyLinkedList<VOConnectedComponent> C2_componentesFuertementeConectados(Digraph grafo) {

		Kosaraju kosaraju = new Kosaraju(grafoDirigidoC1);
		//colores para cada vertice
		ArrayList<Color> arrColores= new ArrayList<>();
		arrColores.add(Color.YELLOW);
		arrColores.add(Color.BLACK);
		arrColores.add(Color.BLUE);
		arrColores.add(Color.PINK);
		arrColores.add(Color.CYAN);
		arrColores.add(Color.GREEN);
		arrColores.add(Color.RED);
		arrColores.add(Color.GRAY);
		VOConnectedComponent[] listaCC = new VOConnectedComponent[kosaraju.count()];
		//Asigna color a los vertices de los componentes  
		for(VOStation st : stationList){
			int componente = kosaraju.id(st.getId());
			VOConnectedComponent componenteconectado = listaCC[componente];
			if(componenteconectado == null){
				componenteconectado = new VOConnectedComponent(arrColores.get(componente));
				componenteconectado.addVertex(st);
			}
			else{
				componenteconectado.addVertex(st);
			}
		}
		//se agregan a una lista los componentes para retornarla 
		lista = new DoublyLinkedList<>();
		for(int i = 0; i < listaCC.length; i++) {
			VOConnectedComponent cc = listaCC[i];
			lista.addAtEnd(cc);
		}
		return lista;
	}

	public void C3_pintarGrafoEstaciones(Digraph grafoEstaciones) {
		//DONE Visualizacion Mapa!
		double viajesPromedioTotales = 0; 
		Iterator <String> iter = elHashDeLosViajes.keys();




		try {
			PrintWriter fr = new PrintWriter("data/C3.html");
			fr.println(BEGIN);
			fr.println("map.on(\"load\", function() {\r\n" + "    map.addSource(\"estaciones\", {\r\n" + "        \"type\": \"geojson\",\r\n" + "        \"data\":{\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [ ");

			for (VOStation st : stationList) {
				fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: ["+ st.getLongitude() + ", " + st.getLatitude() + "]}},\r\n");
			}

			fr.println("{type: 'Feature', geometry: { type: 'Point', coordinates: [0, 0]}}\r\n");
			fr.println("]}});");
			for (int i = 0; i < stationList.getSize(); i++) {	
				for (int j = 0; i < lista.getSize(); i++) {
					
					if(stationList.getElement(i).equals(lista.getElement(0).vertices.getElement(j))) {
					

					double viajesProdNodo = (stationList.getElement(i).getViajesLlegan() + stationList.getElement(i).getViajesSalen())/2;
					viajesPromedioTotales+= viajesProdNodo;
					fr.println("    map.addLayer({\r\n" + "        \"id\": \"inter\",\r\n" + "        \"type\": \"circle\",\r\n" + "        \"source\": \"estaciones\",\r\n" + "        \"paint\": {\r\n" + "            \"circle-radius\":"+viajesProdNodo/viajesPromedioTotales +",\r\n" + "            \"circle-color\": \""+lista.getElement(0).vertices.getElement(j)+"\"\r\n" + "        },\r\n" + "    });");
					fr.println("map.addLayer({\r\n" + "        \"id\": \"route\",\r\n" + "        \"type\": \"line\",\r\n" + "        \"source\": {\r\n" + "            \"type\": \"geojson\",\r\n" + "            \"data\": {\r\n" + "  \"type\": \"FeatureCollection\",\r\n" + "  \"features\": [");

				}	
				}
			}

			while(iter.hasNext()){
				String s = iter.next();
				VOTripsEntreEstaciones t = elHashDeLosViajes.get(s);
				fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [["+t.darEstacionInicio().getLongitude()+","+t.darEstacionInicio().getLatitude()+"],["+t.darEstacionFin().getLongitude()+","+ t.darEstacionFin().getLatitude()+"]]}},\r\n" );				

			}

			
			fr.println("{\"type\": \"Feature\",\"properties\": {},\"geometry\": {\"type\": \"LineString\",\"coordinates\": [[0,0],[0,1]]}}  ]}        },");
			for (int i = 0; i < lista.getSize(); i++) {
				fr.println("\"layout\": {\r\n" + "            \"line-join\": \"round\",\r\n" + "            \"line-cap\": \"round\"\r\n" + "        },\r\n" + "        \"paint\": {\r\n" + "            \"line-color\": \""+lista.getElement(i).getColor()+"\",\r\n" + "            \"line-width\": 2\r\n" + "        }\r\n" + "    });");

			}
			
			fr.println("});" + END);
			fr.close();
			File f = new File ("data/C3.html");


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

	}

	@Override
	public void cargarSistema(String rt, String rs) {
		cargarDatos(rt, rs);
		try {
			cargarJSON();
		} catch (Exception e) {
			e.printStackTrace();
		}
		cargarNodosEstaciones();
	}


}
