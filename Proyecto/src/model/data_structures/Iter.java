package src.model.data_structures;

import java.util.ListIterator;

public class Iter <T> implements ListIterator<T>
{
	Node<T> previous;
	Node<T> current;

	public Iter(Node<T> firstNode)
	{
		current = firstNode;
		previous = null;
	}

	@Override
	public boolean hasNext() 
	{
		// TODO Auto-generated method stub
		return current.getNext() != null;
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		current = current.getNext();
		return current.getElement() ;
	}

	@Override
	public boolean hasPrevious() 
	{
		// TODO Auto-generated method stub
		return current.getPrevious() != null;
	}

	@Override
	public T previous() {
		// TODO Auto-generated method stub
		current = current.getPrevious();
		return current.getElement();
	}

	//=======================================================
	// Unimplemented methods
	//=======================================================

	public int nextIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public int previousIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public void remove() 
	{
		throw new UnsupportedOperationException();
	}

	public void set(T e) 
	{
		throw new UnsupportedOperationException();
	}

	public void add(T e) 
	{
		throw new UnsupportedOperationException();		
	}


}
