package src.model.data_structures;
/**
 * Abstract Data Type for a doubly-linked node of generic objects
 * This ADT should contain the basic operations to manage a node
 *
 *   next, previous, getCurrentElement
 * @param <E>
 */
public interface DoublyLinkedNode<T> {

	 T getNext();
	 T getPrevious();
	 T getElement();
}
