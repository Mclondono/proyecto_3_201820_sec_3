package src.model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
import src.model.data_structures.LinearProbing;
import src.model.data_structures.RedBlackBST;
public class Graph<K,V,A> implements IGraph {
    

    private int vertices;
    private int edges;
    private LinearProbing<String, A> adj;
    private LinearProbing<K, V> v;

    
    /**
     * Initializes an empty graph with 0 vertices and 0 edges.
     */
    public Graph() {
        this.vertices = 0;
        this.edges = 0;
        v = new LinearProbing<K, V>(30000);
        adj = new LinearProbing<String, A>(30000);

    }
    
    public LinearProbing<String, A> getAdj() {
		return adj;
	}
	public LinearProbing<K, V> getV() {
		return v;
	}
    /**
     * Returns the number of vertices in this graph.
     *
     * @return the number of vertices in this graph
     */
    public int V() {
        return vertices;
    }

    /**
     * Returns the number of edges in this graph.
     *
     * @return the number of edges in this graph
     */
    public int E() {
        return edges;
    }
    /**
     * Adds the  vertex to this graph.
     *
     * @param  id the id of the vertex
     * @param  info the info of the vertex (VOPoint or VOStation)
     */
    public void addVertex(K id, V info) {

        vertices++;
        v.put(id, info);      
    }
    
    /**
     * Adds the undirected edge v-w to this graph.
     *
     * @param  idVertexIni one vertex in the edge
     * @param  idVertexFin the other vertex in the edge
     * @param  infoArc the info of the arc (VOEdge)
     */
    public void addEdge(K idVertexIni, K idVertexFin, A infoArc ) {

        edges++;
        adj.put(idVertexIni + "/" + idVertexFin, infoArc);
    }

    //Obtener la informaci�n de un v�rtice
    public V getInfoVertex(K idVertex)	
    {
    	return v.get(idVertex);
    }
    //Modificar la informaci�n del v�rtice idVertex
    public void setInfoVertex(K idVertex, V infoVertex)
    {
    	v.put(idVertex, infoVertex);
    }
    
    //	Obtener la informaci�n de un arco
    public A getInfoArc(K idVertexIni, K idVertexFin)
    {
    	
    	A a = adj.get(idVertexIni + "/" + idVertexFin);
    	if( a == null)
    	{
    		a = adj.get(idVertexFin+ "/" + idVertexIni);
    	}
    	return a;
    }
    
    //Modificar la informaci�n del arco entre los v�rtices idVertexIni e idVertexFin
    public void setInfoArc(K idVertexIni, K idVertexFin, A infoArc)
    {
    	adj.put(idVertexIni + "/" + idVertexFin, infoArc);
    }
    
    //Retorna los identificadores de los v�rtices adyacentes a idVertex
    public Iterator <K> adj (K idVertex)
    {
    	DoublyLinkedList<K> lista = new DoublyLinkedList<>();
    	
    	Iterator <String> iter = adj.keys();
    	while(iter.hasNext())
    	{
    		String[] s = iter.next().split("/");
    		if(s[0].equals(idVertex.toString()))
    		{
    			lista.addAtBeginning((K) s[1]);
    		}
    		else if(s[1].equals(idVertex.toString()))
    		{
    			lista.addAtBeginning((K) s[0]);
    		}
    	}
    	return (Iterator<K>) lista.iterator();  	
    }
    
    /**
     * Retorna una lista con los vertices adyacentes que salen de un nodo (Grafo dirigido).
     */
    public Iterator <K> adjSalen (K idVertex)
    {
    	DoublyLinkedList<K> lista = new DoublyLinkedList<>();
    	
    	Iterator <String> iter = adj.keys();
    	while(iter.hasNext())
    	{
    		String[] s = iter.next().split("/");
    		if(s[0].equals(idVertex.toString()))
    		{
    			lista.addAtBeginning((K) s[1]);
    		}
    	}
    	return (Iterator<K>) lista.iterator();
    	
    }
    
    /**
     * Retorna una lista con los vertices adyacentes que entran a un nodo (Grafo dirigido).
     */
    public Iterator <K> adjEntran (K idVertex)
    {
    	DoublyLinkedList<K> lista = new DoublyLinkedList<>();
    	
    	Iterator <String> iter = adj.keys();
    	while(iter.hasNext())
    	{
    		String[] s = iter.next().split("/");
    		if(s[1].equals(idVertex.toString()))
    		{
    			lista.addAtBeginning((K) s[0]);
    		}
    	}
    	return (Iterator<K>) lista.iterator();
    	
    }

}