package src.model.data_structures;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
@SuppressWarnings("unchecked")
public class MaxHeap <T>
{
	private T [] pq;                
	private int numberOfElements;                 
	private Comparator<T> comparator;  

	/**
	 * Initializes an empty priority queue with the given initial capacity.
	 *
	 * @param  initCapacity the initial capacity of this priority queue
	 */
	public MaxHeap(int initCapacity) {
		pq = (T[]) new Object[initCapacity + 1];
		numberOfElements = 0;
	}

	/**
	 * Initializes an empty priority queue.
	 */
	public MaxHeap()
	{
		this(1);
	}

	/**
	 * Returns true if this priority queue is empty.
	 *
	 * @return True if this priority queue is empty;
	 *         False otherwise
	 */
	public boolean isEmpty() {
		return numberOfElements == 0;
	}

	public T get(int position) {
		// TODO Auto-generated method stub
		return (T) pq[position];
	}
	/**
	 * Returns the number of keys on this priority queue.
	 *
	 * @return the number of keys on this priority queue
	 */
	public int size() {
		return numberOfElements;
	}

	/**
	 * Returns a largest key on this priority queue.
	 *
	 * @return a largest key on this priority queue
	 * @throws NoSuchElementException if this priority queue is empty
	 */
	public T max() {
		if (isEmpty()) throw new NoSuchElementException("Empty priority queue");
		return pq[1];
	}

	// Helper function to double the size of the heap array
	private void resize(int capacity) {
		assert capacity > numberOfElements;
		T[] temp = (T[]) new Object[capacity];
		for (int i = 1; i <= numberOfElements; i++) {
			temp[i] = pq[i];
		}
		pq = temp;
	}


	/**
	 * Adds a new key to this priority queue.
	 *
	 * @param  newKey the new key to add to this priority queue
	 */
	public void insert(T newKey) {

		// double size of array if necessary
		if (numberOfElements == pq.length - 1) resize(2 * pq.length);

		// add x, and percolate it up to maintain heap invariant
		pq[++numberOfElements] = newKey;
		swim(numberOfElements);
		assert isMaxHeap();
	}

	/**
	 * Removes and returns a largest key on this priority queue.
	 *
	 * @return a largest key on this priority queue
	 * @throws NoSuchElementException if this priority queue is empty
	 */
	public T delMax() {
		if (isEmpty()) throw new NoSuchElementException("Priority queue underflow");
		T max = pq[1];
		exch(1, numberOfElements--);
		sink(1);
		pq[numberOfElements+1] = null;     // to avoid loiteing and help with garbage collection
		if ((numberOfElements > 0) && (numberOfElements == (pq.length - 1) / 4)) resize(pq.length / 2);
		assert isMaxHeap();
		return max;
	}

	private void swim(int k) {
		while (k > 1 && less(k/2, k)) {
			exch(k, k/2);
			k = k/2;
		}
	}

	private void sink(int k) {
		while (2*k <= numberOfElements) {
			int j = 2*k;
			if (j < numberOfElements && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

	private boolean less(int i, int j) {
		if (comparator == null) {
			return ((Comparable <T>) pq[i]).compareTo(pq[j]) < 0;
		}
		else {
			return comparator.compare(pq[i], pq[j]) < 0;
		}
	}

	private void exch(int i, int j) {
		T swap = pq[i];
		pq[i] = pq[j];
		pq[j] = swap;
	}

	// is pq[1..N] a max heap?
	private boolean isMaxHeap() {
		return isMaxHeap(1);
	}

	// is subtree of pq[1..n] rooted at k a max heap?
	private boolean isMaxHeap(int k) 
	{
		if (k > numberOfElements) return true;
		int left = 2*k;
		int right = 2*k + 1;
		if (left  <= numberOfElements && less(k, left))  return false;
		if (right <= numberOfElements && less(k, right)) return false;
		return isMaxHeap(left) && isMaxHeap(right);
	}

	/**
	 * Returns an iterator that iterates over the keys on this priority queue
	 * in descending order.
	 * The iterator doesn't implement {@code remove()} since it's optional.
	 *
	 * @return an iterator that iterates over the keys in descending order
	 */
	public Iterator<T> iterator() 
	{
		return new HeapIterator();
	}

	private class HeapIterator implements Iterator<T> 
	{

		// create a new MaxHeap
		private MaxHeap<T> copy;

		// add all items to copy of heap
		public HeapIterator()
		{
			copy = new MaxHeap<T>(size());
			for (int i = 1; i <= numberOfElements; i++)
				copy.insert(pq[i]);
		}

		public boolean hasNext()  
		{ 
			return !copy.isEmpty();                     
		}

		public T next() 
		{
			if (!hasNext()) throw new NoSuchElementException();
			return copy.delMax();
		}
	}
}

