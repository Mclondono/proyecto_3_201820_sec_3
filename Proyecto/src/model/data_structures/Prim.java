package src.model.data_structures;

import src.model.data_structures.Arco;
import src.model.data_structures.IndexMinPQ;
import src.model.data_structures.ListQueue;
import src.model.data_structures.WeightedGraph;

public class Prim {

	    private Arco[] ArcoTo;        // ArcoTo[v] = shortest Arco from tree vertex to non-tree vertex
	    private double[] distTo;      // distTo[v] = weight of shortest such Arco
	    private boolean[] marked;     // marked[v] = true if v on tree, false otherwise
	    private IndexMinPQ<Double> pq;

	    /**
	     * Compute a minimum spanning tree (or forest) of an Arco-weighted graph.
	     * @param G the Arco-weighted graph
	     */
	    public Prim(WeightedGraph G) {
	        ArcoTo = new Arco[G.V()];
	        distTo = new double[G.V()];
	        marked = new boolean[G.V()];
	        pq = new IndexMinPQ<Double>(G.V());
	        for (int v = 0; v < G.V(); v++)
	            distTo[v] = Double.POSITIVE_INFINITY;

	        for (int v = 0; v < G.V(); v++)      // run from each vertex to find
	            if (!marked[v]) prim(G, v);      // minimum spanning forest

	        // check optimality conditions
	    }

	    // run Prim's algorithm in graph G, starting from vertex s
	    private void prim(WeightedGraph G, int s) {
	        distTo[s] = 0.0;
	        pq.insert(s, distTo[s]);
	        while (!pq.isEmpty()) {
	            int v = pq.delMin();
	            scan(G, v);
	        }
	    }

	    // scan vertex v
	    private void scan(WeightedGraph G, int v) {
	        marked[v] = true;
	        for (Arco e : G.adj(v)) {
	            int w = e.other(v);
	            if (marked[w]) continue;         // v-w is obsolete Arco
	            if (e.weight() < distTo[w]) {
	                distTo[w] = e.weight();
	                ArcoTo[w] = e;
	                if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
	                else                pq.insert(w, distTo[w]);
	            }
	        }
	    }

	    /**
	     * Returns the Arcos in a minimum spanning tree (or forest).
	     * @return the Arcos in a minimum spanning tree (or forest) as
	     *    an iterable of Arcos
	     */
	    public Iterable<Arco> Arcos() {
	        ListQueue<Arco> mst = new ListQueue<Arco>();
	        for (int v = 0; v < ArcoTo.length; v++) {
	            Arco e = ArcoTo[v];
	            if (e != null) {
	                mst.enqueue(e);
	            }
	        }
	        return mst;
	    }

	    /**
	     * Returns the sum of the Arco weights in a minimum spanning tree (or forest).
	     * @return the sum of the Arco weights in a minimum spanning tree (or forest)
	     */
	    public double weight() {
	        double weight = 0.0;
	        for (Arco e : Arcos())
	            weight += e.weight();
	        return weight;
	    }


	  
}
