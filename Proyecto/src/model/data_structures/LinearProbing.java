package src.model.data_structures;

import java.util.Iterator;

public class LinearProbing<K,V> implements GenericHashTable<K,V>{

	 	private int n;           // number of key-value pairs in the symbol table
	    private int m;           // size of linear probing table
	    private K[] keys;      // the keys
		private V[] vals;    // the values
	
	    public LinearProbing(int capacity)
	    {
	    	m = capacity;
	        n = 0;
	        keys = (K[])   new Object[m];
	        vals = (V[]) new Object[m];
	    }
	@Override
	public void put(K k, V v) {
		if (k == null) throw new IllegalArgumentException("first argument to put() is null");

        if (v == null) {
            delete(k);
            return;
        }

        // double table size if 75% full
        if (n >= 	3* m/4) resize(2*m);

        int i;
        for (i = hash(k); keys[i] != null; i = (i + 1) % m) {
            if (keys[i].equals(k)) {
                vals[i] = v;
                return;
            }
        }
        keys[i] = k;
        vals[i] = v;
        n++;
	}
	
	// resizes the hash table to the given capacity by re-hashing all of the keys
    private void resize(int capacity) {
        LinearProbing<K, V> temp = new LinearProbing<K, V>(capacity);
        for (int i = 0; i < m; i++) {
            if (keys[i] != null) {
                temp.put(keys[i], vals[i]);
            }
        }
        keys = temp.keys;
        vals = temp.vals;
        m    = temp.m;
    }
    // hash function for keys - returns value between 0 and M-1
    private int hash(K key) {
        return (key.hashCode() & 0x7fffffff) % m;
    }

	@Override
	public V get(K k) {
		if (k == null) throw new IllegalArgumentException("argument to get() is null");
        for (int i = hash(k); keys[i] != null; i = (i + 1) % m)
            if (keys[i].equals(k))
                return vals[i];
        return null;
	}

	@Override
	public V delete(K k) {
		if (k == null) throw new IllegalArgumentException("argument to delete() is null");
        if (!contains(k)) return null;

        // find position i of key
        int i = hash(k);
        while (!k.equals(keys[i])) {
            i = (i + 1) % m;
        }
        V toDelete = vals[i];
        // delete key and associated value
        keys[i] = null;
        vals[i] = null;

        // rehash all keys in same cluster
        i = (i + 1) % m;
        while (keys[i] != null) {
            // delete keys[i] an vals[i] and reinsert
            K   keyToRehash = keys[i];
            V valToRehash = vals[i];
            keys[i] = null;
            vals[i] = null;
            n--;
            put(keyToRehash, valToRehash);
            i = (i + 1) % m;
        }

        n--;
        return toDelete;
	}
	 public boolean contains(K key) {
	        if (key == null) throw new IllegalArgumentException("argument to contains() is null");
	        return get(key) != null;
	    }

	@Override
	public Iterator<K> keys() {
		ListQueue<K> queue = new ListQueue<K>();
        for (int i = 0; i < m; i++)
            if (keys[i] != null) queue.enqueue(keys[i]);
        return  queue.iterator();
	}
    public K[] getKeys() {
		return keys;
	}
	public void setKeys(K[] keys) {
		this.keys = keys;
	}
	public V[] getVals() {
		return vals;
	}
	public void setVals(V[] vals) {
		this.vals = vals;
	}

}
