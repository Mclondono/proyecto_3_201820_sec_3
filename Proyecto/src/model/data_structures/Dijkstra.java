package src.model.data_structures;

import src.model.data_structures.Arco;
import src.model.data_structures.IndexMinPQ;
import src.model.data_structures.ListStack;
import src.model.data_structures.WeightedGraph;

public class Dijkstra {
    private double[] distTo;          // distTo[v] = distance  of shortest s->v path
    private Arco[] ArcoTo;            // ArcoTo[v] = last Arco on shortest s->v path
    private IndexMinPQ<Double> pq;    // priority queue of vertices

    /**
     * Computes a shortest-paths tree from the source vertex {@code s} to every
     * other vertex in the Arco-weighted graph {@code G}.
     *
     * @param  G the Arco-weighted digraph
     * @param  s the source vertex
     * @throws IllegalArgumentException if an Arco weight is negative
     * @throws IllegalArgumentException unless {@code 0 <= s < V}
     */
    public Dijkstra(WeightedGraph G, int s) {
        for (Arco e : G.Arcos()) {
            if (e.weight() < 0)
                throw new IllegalArgumentException("Arco " + e + " has negative weight");
        }

        distTo = new double[G.V()];
        ArcoTo = new Arco[G.V()];

        validateVertex(s);

        for (int v = 0; v < G.V(); v++)
            distTo[v] = Double.POSITIVE_INFINITY;
        distTo[s] = 0.0;

        // relax vertices in order of distance from s
        pq = new IndexMinPQ<Double>(G.V());
        pq.insert(s, distTo[s]);
        while (!pq.isEmpty()) {
            int v = pq.delMin();
            for (Arco e : G.adj(v))
                relax(e, v);
        }

        // check optimality conditions
        assert check(G, s);
    }

    // relax Arco e and update pq if changed
    private void relax(Arco e, int v) {
        int w = e.other(v);
        if (distTo[w] > distTo[v] + e.weight()) {
            distTo[w] = distTo[v] + e.weight();
            ArcoTo[w] = e;
            if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
            else                pq.insert(w, distTo[w]);
        }
    }

    /**
     * Returns the length of a shortest path between the source vertex {@code s} and
     * vertex {@code v}.
     *
     * @param  v the destination vertex
     * @return the length of a shortest path between the source vertex {@code s} and
     *         the vertex {@code v}; {@code Double.POSITIVE_INFINITY} if no such path
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public double distTo(int v) {
        validateVertex(v);
        return distTo[v];
    }

    /**
     * Returns true if there is a path between the source vertex {@code s} and
     * vertex {@code v}.
     *
     * @param  v the destination vertex
     * @return {@code true} if there is a path between the source vertex
     *         {@code s} to vertex {@code v}; {@code false} otherwise
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public boolean hasPathTo(int v) {
        validateVertex(v);
        return distTo[v] < Double.POSITIVE_INFINITY;
    }

    /**
     * Returns a shortest path between the source vertex {@code s} and vertex {@code v}.
     *
     * @param  v the destination vertex
     * @return a shortest path between the source vertex {@code s} and vertex {@code v};
     *         {@code null} if no such path
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public Iterable<Arco> pathTo(int v) {
        validateVertex(v);
        if (!hasPathTo(v)) return null;
        ListStack<Arco> path = new ListStack<Arco>();
        int x = v;
        for (Arco e = ArcoTo[v]; e != null; e = ArcoTo[x]) {
            path.push(e);
            x = e.other(x);
        }
        return path;
    }


    // check optimality conditions:
    // (i) for all Arcos e = v-w:            distTo[w] <= distTo[v] + e.weight()
    // (ii) for all Arco e = v-w on the SPT: distTo[w] == distTo[v] + e.weight()
    private boolean check(WeightedGraph G, int s) {

        // check that Arco weights are nonnegative
        for (Arco e : G.Arcos()) {
            if (e.weight() < 0) {
                System.err.println("negative Arco weight detected");
                return false;
            }
        }

        // check that distTo[v] and ArcoTo[v] are consistent
        if (distTo[s] != 0.0 || ArcoTo[s] != null) {
            System.err.println("distTo[s] and ArcoTo[s] inconsistent");
            return false;
        }
        for (int v = 0; v < G.V(); v++) {
            if (v == s) continue;
            if (ArcoTo[v] == null && distTo[v] != Double.POSITIVE_INFINITY) {
                System.err.println("distTo[] and ArcoTo[] inconsistent");
                return false;
            }
        }

        // check that all Arcos e = v-w satisfy distTo[w] <= distTo[v] + e.weight()
        for (int v = 0; v < G.V(); v++) {
            for (Arco e : G.adj(v)) {
                int w = e.other(v);
                if (distTo[v] + e.weight() < distTo[w]) {
                    System.err.println("Arco " + e + " not relaxed");
                    return false;
                }
            }
        }

        // check that all Arcos e = v-w on SPT satisfy distTo[w] == distTo[v] + e.weight()
        for (int w = 0; w < G.V(); w++) {
            if (ArcoTo[w] == null) continue;
            Arco e = ArcoTo[w];
            if (w != e.either() && w != e.other(e.either())) return false;
            int v = e.other(w);
            if (distTo[v] + e.weight() != distTo[w]) {
                System.err.println("Arco " + e + " on shortest path not tight");
                return false;
            }
        }
        return true;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        int V = distTo.length;
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }

    
    
    

}

