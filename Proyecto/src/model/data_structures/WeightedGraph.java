package src.model.data_structures;

import src.model.vo.VOEdge;
import src.model.vo.VOPoint;

public class WeightedGraph {
	private final int V; // number of vertices
	private int E; // number of edges
	private Bag<Arco>[] adj; // adjacency lists
	private  Bag<VOEdge>[] adj1;
	
	public WeightedGraph(int V)
	{
		this.V = V;
		this.E = 0;
		adj = (Bag<Arco>[]) new Bag[V];
		for (int v = 0; v < V; v++)
			adj[v] = new Bag<Arco>();
		
		adj1 = (Bag<VOEdge>[]) new Bag[V];
		for (int v = 0; v < V; v++)
			adj1[v] = new Bag<VOEdge>();
	}
	
	public int V() { return V; }
	public int E() { return E; }
	public void addEdge(Arco e)
	{
		int v = e.either(), w = e.other(v);
		adj[v].add(e);
		adj[w].add(e);
		E++;
	}
	public Iterable<Arco> adj(int v)
	{ 
		return adj[v]; 
	}
	public Iterable<VOEdge> adj1(VOEdge v)
	{ 
		return adj1[v.getPunto1().getId()]; 
	}
	public Iterable<Arco> Arcos(){
		Bag<Arco> b = new Bag<Arco>();
		for (int v = 0; v < V; v++)
		for (Arco e : adj[v])
		if (e.other(v) > v) b.add(e);
		return b;
	}
	
	public Iterable<VOEdge> Arcos1(){
		Bag<VOEdge> b = new Bag<VOEdge>();
		for (int v = 0; v < V; v++)
		for (VOEdge e : adj1[v])
		if (e.getPunto1().getId() > v) b.add(e);
		return b;
	}

	public void addArc(Arco arco) {
		int v = arco.either();
        int w = arco.other(v);
        try {
			validateVertex(v);
	        validateVertex(w);
		} catch (Exception e) {
			e.printStackTrace();
		}
        adj[v].add(arco);
        adj[w].add(arco);
        E++;		
	}

	private void validateVertex(int w) throws Exception {
		if (w < 0 || w >= V)
            throw new Exception("problema validando vertex");
	}

	public void addEdge(VOEdge a) {
		// TODO Auto-generated method stub
		VOPoint v = a.getPunto1(), w = a.getPunto2();
		adj1[v.getId()].add(a);
		adj1[w.getId()].add(a);
		E++;
		
	}
}