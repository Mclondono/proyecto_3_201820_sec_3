package src.model.data_structures;

import java.util.Iterator;

import src.model.vo.VOEdge;
import src.model.vo.VOPoint;

public class PrimMST<T extends Comparable<T>>
{
	private Arco[] edgeTo; // shortest edge from tree vertex
	private double[] distTo; // distTo[w] = edgeTo[w].weight()
	private boolean[] marked; // true if v on tree
	private IndexMinPQ<Double> pq; // eligible crossing edges
	private int weight; 
	private VOEdge[] edgeTo1;
	private IndexMinPQ pq1;
	public PrimMST(WeightedGraph G)
	{
		weight = 0; 
		edgeTo = new Arco[G.V()];
		edgeTo1 = new VOEdge[G.V()];
		distTo = new double[G.V()];
		marked = new boolean[G.V()];
		for (int v = 0; v < G.V(); v++)
			distTo[v] = Double.POSITIVE_INFINITY;
		pq = new IndexMinPQ<Double>(G.V());
		pq1 = new IndexMinPQ(G.V());
		distTo[0] = 0.0;
		pq.insert(0, 0.0); // Initialize pq with 0, weight 0.
		while (!pq.isEmpty())
			visit(G, pq.delMin()); // Add closest vertex to tree.
		
		pq1.insert(G.Arcos1().iterator().next().getPunto1().getId(), Double.toString(G.Arcos1().iterator().next().getPeso())  );
		while (!pq1.isEmpty())
			visit(G, pq1.delMin()); // Add closest vertex to tree.
	}
	private void visit(WeightedGraph G, int v)
	{ // Add v to tree; update data structures.
		
		marked[v] = true;
		for (Arco e : G.adj(v))
		{
			int w = e.other(v);
			if (marked[w]) continue; // v-w is ineligible.
			if (e.weight() < distTo[w])
			{ // Edge e is new best connection from tree to w.
				edgeTo[w] = e;
				distTo[w] = e.weight();
				weight+=distTo[w];
				if (pq.contains(w)) pq.change(w, distTo[w]);
				else pq.insert(w, distTo[w]);
			}
		}
	}
	
	public Iterable<Arco> edges() {
		Bag<Arco> mst = new Bag<Arco>();
		for (int v = 1; v < edgeTo.length; v++)
		mst.add(edgeTo[v]);
		return mst;
	}
	public Iterable<VOEdge> edges1() {
		Bag<VOEdge> mst = new Bag<VOEdge>();
		for (int v = 1; v < edgeTo1.length; v++)
		mst.add(edgeTo1[v]);
		return mst;
	}
	public Iterator<VOEdge> iterator1(){
		return edges1().iterator();
		
	}
	
	private void visit1(WeightedGraph G, VOEdge v)
	{ // Add v to tree; update data structures.
		
		marked[v.getPunto1().getId()] = true;
		for (VOEdge e : G.adj1(v))
		{
			VOPoint w = e.getPunto1();
			if (marked[w.getId()]) continue; // v-w is ineligible.
			if (e.getPeso() < distTo[w.getId()])
			{ // Edge e is new best connection from tree to w.
				edgeTo1[w.getId()] = e;
				distTo[w.getId()] = e.getPeso();
				weight+=distTo[w.getId()];
				if (pq.contains(w.getId())) pq.change(w.getId(), distTo[w.getId()]);
				else pq.insert(w.getId(), distTo[w.getId()]);
			}
		}
	}
	public double weight() {
		return weight; 
	}

}
