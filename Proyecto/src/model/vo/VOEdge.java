package src.model.vo;

public class VOEdge  {

	
	private VOPoint punto1;
	private VOPoint punto2;
	private double peso;
	private VOStation relacionEstacion;
	public VOEdge(VOPoint pPunto1, VOPoint pPunto2, double pPeso)
	{
		setPunto1(pPunto1);
		setPunto2(pPunto2);
		setPeso(pPeso);
	}
	public VOEdge(VOPoint pPunto1, VOStation pRelacionEstacion, double pPeso)
	{
		setPunto1(pPunto1);
		setRelacionEstacion(pRelacionEstacion);
		setPeso(pPeso);
	}
	public VOPoint getPunto1() {
		return punto1;
	}
	public void setPunto1(VOPoint punto1) {
		this.punto1 = punto1;
	}
	public VOPoint getPunto2() {
		return punto2;
	}
	public void setPunto2(VOPoint punto2) {
		this.punto2 = punto2;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public VOStation getRelacionEstacion() {
		return relacionEstacion;
	}
	public void setRelacionEstacion(VOStation relacionEstacion) {
		this.relacionEstacion = relacionEstacion;
	}
	
	
}
