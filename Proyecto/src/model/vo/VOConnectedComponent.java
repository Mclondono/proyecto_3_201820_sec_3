package src.model.vo;

import java.awt.Color;

import src.model.data_structures.DoublyLinkedList;

public class VOConnectedComponent 
{
	public Color color;
	
	public DoublyLinkedList<VOStation> vertices;
	
	public VOConnectedComponent(Color pColor)
	{
		color = pColor;
		vertices = new DoublyLinkedList<>();
	}
	
	public Color getColor()
	{
		return color;
	}
	
	public void addVertex(VOStation pStation)
	{
		vertices.addAtBeginning(pStation);
	}

}
