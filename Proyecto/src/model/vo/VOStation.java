package src.model.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a station object
 */
public class VOStation implements Comparable<VOStation>{

private int id;
private String name;
private String city;
private double latitude;
private double longitude;
private int dpcapacity;
private Date online_date;
private int viajesSalen;
private int viajesLlegan;
private int cSalida;
private int cLlegada;

public VOStation(int pid, String pname, String pcity, double platitude, double plongitude, int pdpcapacity, String ponline_date) throws Exception
{
	cSalida=0;
	cLlegada=0;
	id = pid;
	name = pname;
	city = pcity;
	latitude = platitude;
	longitude = plongitude;
	dpcapacity = pdpcapacity;
	online_date =  new SimpleDateFormat ("MM/dd/yyyy kk:mm").parse(ponline_date);
}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getCity() {
		return city;
	}
	public double getLatitude() {
		return latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public int getdpCapacity() {
		return dpcapacity;
	}
	public Date getonline_date() {
		return online_date;
	}
	public int getViajesSalen() {
		return viajesSalen;
	}
	public void setViajesSalen(int viajesSalen) {
		this.viajesSalen = viajesSalen;
	}
	public int getViajesLlegan() {
		return viajesLlegan;
	}
	public void setViajesLlegan(int viajesLlegan) {
		this.viajesLlegan = viajesLlegan;
	}
	@Override
	public int compareTo(VOStation o) {
		return 0;
	}
	public void cSalidaMas() {
		cSalida++;		
	}
	public void cLlegadaMas() {
		cLlegada++;
	}
}
