package src.model.vo;

public class VOTripsEntreEstaciones {

	private VOStation inicio;
	private VOStation fin;
	private int numviajes;
	private int tiempototal;
	private int generoDominante; // Este numerto es positivo si hay m�s hombres en este recorrido, negativo si hay m�s mujeres y cero si son iguales
	
	public VOTripsEntreEstaciones( VOStation pinicio, VOStation pfin)
	{
		inicio = pinicio;
		fin = pfin;
		numviajes = 0;
		tiempototal = 0;
	}
	
	public void aumentarNumViajes()
	{
		numviajes++;
	}
	public void genero(String gen)
	{
		if (gen.equals(VOTrip.MALE))
		{
			setGeneroDominante(getGeneroDominante() + 1);
		}
		else if (gen.equals(VOTrip.FEMALE))
		{
			setGeneroDominante(getGeneroDominante() - 1);
		}
	}
	public void aumentarTiempoTotal(int ptiempo)
	{
		tiempototal += ptiempo;
	}
	public VOStation darEstacionInicio()
	{
		return inicio;
	}
	public VOStation darEstacionFin()
	{
		return fin;
	}
	public int darNumViajes()
	{
		return numviajes;
	}
	public int darTiempoTotal()
	{
		return tiempototal;
	}
	public double darpromedio()
	{
		if(numviajes == 0)
		{
			return 0;
		}
		else
		{
			return tiempototal/numviajes;
		}
	}

	public int getGeneroDominante() {
		return generoDominante;
	}

	public void setGeneroDominante(int generoDominante) {
		this.generoDominante = generoDominante;
	}
}
