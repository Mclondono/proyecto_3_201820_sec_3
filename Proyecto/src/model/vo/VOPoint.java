package src.model.vo;

public class VOPoint implements Comparable<VOPoint>{

	private int id;
	private double longitude;
	private double latitude;
	private boolean esEstacion;
	public VOPoint(int pId, double pLongitude , double pLatitude, boolean pEstacion)
	{
		setId(pId);
		setLongitude(pLongitude);
		setLatitude(pLatitude);
		setEstacion(pEstacion);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public boolean isEstacion() {
		return esEstacion;
	}
	public void setEstacion(boolean esEstacion) {
		this.esEstacion = esEstacion;
	}
	@Override
	public int compareTo(VOPoint o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
