package src.model.vo;

import src.model.data_structures.DoublyLinkedList;
import src.model.data_structures.IDoublyLinkedList;

public class VOPath<T> implements Comparable<VOPath>
{
	
	
	//Definir un atributo lista de nodos o HashTable de nodos o cualquier otra forma que les permita 
	//--- almacenar la informacion
	private IDoublyLinkedList<T> listaNodos;
	//Definir un atributo distancia total de un camino(path)
	private double distTotal;
	
	public VOPath(){
		distTotal=0;
		listaNodos=new DoublyLinkedList<>();
	}
	
	public void addListaNodos(IDoublyLinkedList<T> l){
		listaNodos = l;
	}
	public IDoublyLinkedList<T>  darListaNodos(){
		return listaNodos;
	}
	public void addNodoInicio(T t){
		listaNodos.addAtBeginning(t);
	}
	public void addNodoFinal(T t){
		listaNodos.addAtEnd(t);
	}
	public void addDist(double d){
		distTotal+= d;
	}
	public int compareTo(VOPath o) 
	{
		//recomendamos comparar paths por distancia total.
		if(o.distTotal == this.distTotal) {
			return 0; 
		}
		
		else if(o.distTotal > this.distTotal) {
			return 1; 
		}
		else
			return -1; 
	}

	public double getTotalDistance() {
		return distTotal;
	}
}
